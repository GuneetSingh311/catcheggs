//
//  GameScene.swift
//  Eggs
//
//  Created by Admin on 2019-02-08.
//  Copyright © 2019 Guneet SIngh Lamba. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {

    let basket = SKSpriteNode(imageNamed: "basket-2.png")
    var counter:CGFloat = 0.0
    var eggSpeed:CGFloat  = 2.0
    var egg = SKSpriteNode(imageNamed: "egg.png")
    var hen = SKSpriteNode(imageNamed: "hen.png")
    let scoreLabel = SKLabelNode(text: "")
    var scores:Int = 0
    override func didMove(to view: SKView)
    {
      
        scene?.backgroundColor = UIColor.white
        scoreLabel.position = CGPoint(x: size.width/2 , y: size.height/2)
        basket.position = CGPoint(x: size.width/2, y: 80)
       // basket.zPosition =  -1
        scoreLabel.fontColor = UIColor.red
        scoreLabel.fontSize = 40.0
       // spawnEggs()
        run(SKAction.repeatForever(
            SKAction.sequence([
                SKAction.run(spawnEggs),
                SKAction.wait(forDuration: 2.0)
                ])
            ))
        
       // Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(spawnEggs), userInfo: nil, repeats: true)
        
        addChild(basket)
        addChild(scoreLabel)
        
}
    
    @objc func spawnEggs() {
        
        egg = SKSpriteNode(imageNamed: "egg.png")
        hen = SKSpriteNode(imageNamed: "hen.png")
        egg.position = CGPoint(x: (size.width/2)+CGFloat.random(in: -60...70), y: size.height-100)
        addChild(egg)
        hen.position = CGPoint(x: egg.position.x, y:size.height-80)
        addChild(hen)
      
        egg.physicsBody?.affectedByGravity  = true
        egg.physicsBody = SKPhysicsBody(rectangleOf: egg.frame.size)

       }
    
    

    override func update(_ currentTime: TimeInterval) {
       
      
        
        if(egg.frame.intersects(basket.frame))
        {
            self.scores += 1
            self.scoreLabel.text = "\(scores)"
          egg.removeFromParent()
         hen.removeFromParent()
        }
        
       if(egg.position.y < 0)
        {
            finalscores = self.scores
            self.isPaused = true
            print("Missed")
            // 1. Initialize the new scene
            let gameOverScene = GameOverScene(size:self.size)
            gameOverScene.scaleMode = self.scaleMode
            //
            //            // 2. Configure the "animation" between screens
            let transitionEffect =  SKTransition.push(with: SKTransitionDirection.up, duration: 1)
            
          
            //
            //            // 3. Show the scene
            self.view?.presentScene(gameOverScene, transition: transitionEffect)
        }
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let location = touch.location(in:self)
            let middleofScreen = self.size.width/2
            if(location.x<middleofScreen)
            {
                print("tocuhed the left side")
                let moveBasketLeft = SKAction.moveTo(x: 60, duration: 1)
               basket.run(moveBasketLeft)
            }
            else {
                print("touched right")
                let moveBasketRight = SKAction.moveTo(x: size.width-60, duration: 1)
                basket.run(moveBasketRight)
            }
        }
        
    }
}

    
    

