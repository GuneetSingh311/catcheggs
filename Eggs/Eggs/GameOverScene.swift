//
//  GameOverScene.swift
//  ZombieConga
//
//  Created by MacStudent on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import SpriteKit
var image = "YouWin"
var finalscores = 0

// The default template for a new scssene
class GameOverScene:SKScene {

    let restart = SKSpriteNode(imageNamed: "restart.png")
    let scoreLabel = SKLabelNode(text: "Eggs Collected:")
    override func didMove(to view: SKView) {
        scene?.backgroundColor = UIColor.white
            restart.position = CGPoint(x: size.width/2, y: size.height/2)
            scoreLabel.position = CGPoint(x: size.width/2, y: size.height-100)
        scoreLabel.text = "Score: \(finalscores)"
        scoreLabel.fontSize  = 40.0
        scoreLabel.fontColor = UIColor.black
        scoreLabel.fontName = UIFont.TextStyle.largeTitle.rawValue
        addChild(restart)
        addChild(scoreLabel)
}
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        for touch in touches {
            let location = touch.location(in: self)
            
            if(restart.contains(location))
            {
                // When person touches screen, send them back to the game
                
                // 1. Initialize the new scene
                
                let gameScene = GameScene(size:self.size)
                gameScene.scaleMode = self.scaleMode
                
                // 2. Configure the "animation" between screens
                let transitionEffect =  SKTransition.push(with: SKTransitionDirection.down, duration: 1)
                
                
                //
                //            // 3. Show the scene
                self.view?.presentScene(gameScene, transition: transitionEffect)
                
            }
        }
        
       
    }
}

