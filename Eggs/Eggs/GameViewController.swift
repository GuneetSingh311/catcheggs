//
//  GameViewController.swift
//  Eggs
//
//  Created by Admin on 2019-02-08.
//  Copyright © 2019 Guneet SIngh Lamba. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
var gravityConstant = -5
class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = GameScene(size:view.frame.size)
        let skView = view as! SKView
        scene.backgroundColor = UIColor.white        // add some debug info the screen
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.ignoresSiblingOrder = false
        scene.scaleMode = .aspectFill
        
        skView.showsPhysics = false
        
        // Configure the gravity in our world
        scene.physicsWorld.gravity = CGVector(dx: 0, dy: gravityConstant)
        
        // present the scene
        skView.presentScene(scene)
        
        
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
