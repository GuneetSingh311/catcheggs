

## Eggs Game

Application is made in swift language using Sprite Kit for ios platform

1. Application uses touch gestures.
2. Application uses spritekit for animating sprites
3. Score is based on the collection of eggs in basket
4. Game speed is increased when user reaches at certain level
![Alt text](https://bitbucket.org/GuneetSingh311/catcheggs/downloads/1.png)
![Alt text](https://bitbucket.org/GuneetSingh311/catcheggs/downloads/2.png)
![Alt text](https://bitbucket.org/GuneetSingh311/catcheggs/downloads/3.png)